document.addEventListener("DOMContentLoanded", ()=>{
    var pret=slider.noUiSlider.get();
    document.getElementById("MenuPriceLeft").innerHTML="$"+pret[0];
    document.getElementById("MenuPriceRight").innerHTML="-$"+pret[1];
});

var slider = document.getElementById('slider');

noUiSlider.create(slider, {
    start: [20000, 70000],
    connect: true,
    range: {
        'min': 1000,
        'max': 100000
    },
    format: wNumb({
        decimals: 0
    })
});

document.getElementById('slider').addEventListener('click',function(){
    var pret=slider.noUiSlider.get();
    document.getElementById("MenuPriceLeft").innerHTML="$"+pret[0];
    document.getElementById("MenuPriceRight").innerHTML="-$"+pret[1];
});

var slideImage=1;
function prevImage(){

    if(slideImage==1)
    {
        document.getElementById('backImage').src="Images/hero_bg_3.jpg";
        document.getElementById('Tname').innerHTML="625 S. Berendo St";
        document.getElementById('Tlocation').innerHTML="<i class=\"fas fa-map-marker-alt\"></i> 607 Los Angeles, CA 90005";
        slideImage++;

    }
    else {
        document.getElementById('backImage').src="Images/hero_bg_1.jpg";
        document.getElementById('Tname').innerHTML="835 S Lucerne Blvd";
        document.getElementById('Tlocation').innerHTML="<i class=\"fas fa-map-marker-alt\"></i> Los Angeles, CA 90005";
        slideImage--;
    }
}

$(document).ready(function() {
    $(".dropdown-toggle").dropdown();
});